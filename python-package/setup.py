#setup.py
from distutils.core import setup
from distutils.extension import Extension
from Cython.Distutils import build_ext

setup(
    cmdclass = {'build_ext': build_ext},
    ext_modules = [
        Extension("lffm", ["_lffm.pyx","../ffm.cpp","../ffm-train.cpp","../ffm-predict.cpp"]
		 ,language="c++",
		extra_compile_args=['-std=c++11','-msse3'],
		extra_link_args=['-std=c++11'])
        ]
)
