cdef extern from "../ffm.h":
	cdef extern void py_train(char a[])
	cdef extern void py_predict(char a[])

def train(s):
	py_train(s);
def predict(s):
	py_predict(s);
